import org.junit.Assert;
import org.junit.Test;

import static edu.gvsu.dlunit.DLUnit.*;

  public class ALUTest{

  private void verifyMultiplication(long a, long b) {

    long expected = a * b;
    boolean expectedOverflow = ((expected >= (1 << 15)) || (expected < -(1 << 15)));

    setPinSigned("InputA", a);
    setPinSigned("InputB", b);
    run();
    String message = String.format("of %d x %d: ", a, b);

    if (expectedOverflow) {
      Assert.assertEquals("Low " + message, expected & 0xFFFF, readPinUnsigned("Low"));
      Assert.assertEquals("High " + message, (expected >> 16) & 0x0FFFFL, readPinUnsigned("High"));
    } else {
      Assert.assertEquals("Low " + message, expected, readPinSigned("Low"));
      long expectedHigh = expected >= 0 ? 0 : -1;
      Assert.assertEquals("High " + message, expectedHigh, readPinSigned("High"));
    }
    Assert.assertEquals("Overflow " + message, expectedOverflow, readPin("Overflow"));
  }

  private void verifySignedAddSubtract(long a, long b, boolean op){
  	setPinSigned("InputA", a);
    setPinSigned("InputB", b);
    setPin("Op", op);
    run();
    String message = "of" + a + (op ? " + " : " - ") + b + ": ";

    // Output "wraps around" if there is an overflow
    if (expectedOverflow && expected > 0) {
      expected -= 65536;
    } else if (expectedOverflow && expected < 0) {
      expected += 65536;
    }
    Assert.assertEquals("Output " + message, expected, readPinSigned("Output"));
    Assert.assertEquals("Overflow " + message, expectedOverflow, readPin("Overflow"));
  }

  /*
  * Receives as a parameter numbers a and b to add together
  * Then an op code representing either addition = 0, subtraction = 1, multiplication = 4
  * Computes the expected result and sees what the circuit output is compared to what we expected.
  */
  private void verifySignedAddition(long a, long b, boolean op){
  	long carryInAsInt = (carryIn ? 1 : 0);
    long expected = a + b + carryInAsInt;
    boolean expectedOverflow = ((expected >= (1 << 15)) || (expected < -(1 << 15)));

    setPinSigned("InputA", a);
    setPinSigned("InputB", b);
    setPinSigned("Op", op);
    run();
    String message = "of " + a + " + " + b + " with " + (carryIn ? "a " : " no ") + " carry in";

    // Output "wraps around" if there is an overflow
    if (expectedOverflow && expected > 0) {
      expected -= 65536;
    } else if (expectedOverflow && expected < 0) {
      expected += 65536;
    }
    Assert.assertEquals("Output " + message, expected, readPinSigned("Output"));
    Assert.assertEquals("Overflow " + message, expectedOverflow, readPin("Overflow"));
  }

  /*
  * Testing the adding of two positive numbers.
  */
  @Test
  public void testPosPosAdd(){
     verifySignedAddition(0, 1, 0);
  }

  @Test
  public void testPosPosAdd2(){
     verifySignedAddtion(1, -1,0);
  }

  @Test
  public void testPosPosAdd3(){
     verifySignedAddtion(0,-1,0);
  }
}

